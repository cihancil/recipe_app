import React, { Component } from 'react';
import { Provider } from 'mobx-react/native';
import AppStore from './stores/app-store';
import HomeStore from './stores/home-store';
import CategoriesStore from './stores/categories-store';
import SearchStore from './stores/search-store';
import RootContainer from './containers/root-container';

const store = {
  app: AppStore,
  home: HomeStore,
  categories: CategoriesStore,
  search: SearchStore,
};

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
    return (
      <Provider store={store}>
        <RootContainer />
      </Provider>
    )
  }
}
