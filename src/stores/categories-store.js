import { observable, computed, action } from 'mobx';

import Api from '../services/api';
import BaseFetcherStore from './base-fetcher-store';

class CategoriesStore extends BaseFetcherStore {

  @observable _categories = []

  @computed get categories() {
    return this._categories.slice();
  }

  @action fetchCategories() {
    return new Promise((resolve, reject) => {
      this._fetching = true;
      return Api.fetchCategories()
        .then(fetchedCategory => {
          this._categories = fetchedCategory;
          this._fetching = false;
          resolve();
        })
    })
      .catch(err => {
        this._fetching = false;
      })
  }
}

export default new CategoriesStore();