import { observable, computed, action } from 'mobx';

import Api from '../services/api';
import BaseFetcherStore from './base-fetcher-store';

class HomeStore extends BaseFetcherStore {

  @observable _recipes = []

  @action fetchHomeFeed() {
    return new Promise((resolve, reject) => {
      this._fetching = true;
      return Api.fetchHomeFeed()
        .then(homeFeed => {
          let newRecipes = [];
          let headlines = homeFeed.featured.concat(homeFeed.trend);
          newRecipes.push({
            type: "headlines",
            recipes: headlines,
          });


          homeFeed.latest.forEach((item, index) => {
            if (index % 3 === 0) {
              newRecipes.push({
                type: "single",
                recipe: item,
              });
            }
            if (index % 3 === 1) {
              let items = [];
              items.push(item);
              if (homeFeed.latest[index + 1]) {
                items.push(homeFeed.latest[index + 1]);
              }
              newRecipes.push({
                type: "double",
                recipes: items,
              });
            }
          })
          this._recipes = newRecipes;
          this._fetching = false;
          resolve();
        })
        .catch(err => {
          this._fetching = false;
        })
    })
  }

  @action refreshHomeFeed() {
    return new Promise((resolve, reject) => {
      this._refreshing = true;
      return this.fetchHomeFeed()
        .then(() => {
          setTimeout(() => {
            this._refreshing = false;
          }, 300);
          resolve();
        })
    })
      .catch(err => {
        this._refreshing = false;
        reject();
      })
  }
}

export default new HomeStore();