import { observable, computed, action } from 'mobx';

import Api from '../services/api';
import BaseFetcherStore from './base-fetcher-store';

class SearchStore extends BaseFetcherStore {

  @observable _discoveryItems = []

  @computed get discoveryItems() {
    return this._discoveryItems.slice();
  }

  @action fetchDiscovery() {
    return new Promise((resolve, reject) => {
      this._fetching = true;

      Api.fetchDiscovery()
        .then(discoveryItems => {
          this._fetching = false;
          this._discoveryItems = discoveryItems;
        })
        .catch(err => {
          this._fetching = false;
          reject();
        })
    })
  }

}

export default new SearchStore();