import { observable, computed, action } from 'mobx';

import Api from '../services/api';
import BaseFetcherStore from './base-fetcher-store';

class CategoryFeedStore extends BaseFetcherStore {
  constructor(id) {
    super();
    this.id = id;
  }

  @observable _recipes = []
  @observable _page = 1

  @computed get recipes() {
    return this._recipes.slice();
  }

  @computed get canFetchMore() {
    return this._page < 10;
  }

  @action fetchCategoryFeed() {
    return new Promise((resolve, reject) => {
      if (!this.canFetchMore) {
        return;
      }
      this._fetching = true;
      return Api.fetchCategoryFeed({
        id: this.id,
        page: this._page,
      })
        .then(fetchedRecipes => {
          if (false && this._page === 1) {
            this._recipes = fetchedRecipes;
          } else {
            this._recipes = this._recipes.concat(fetchedRecipes);
          }
          this._fetching = false;
          this._page++;
          resolve();
        })
    })
      .catch(err => {
        this._fetching = false;
      })
  }
}

export default CategoryFeedStore;