import { observable, computed, action } from 'mobx';

class BaseFetcherStore {

  @observable _fetching = false
  @observable _refreshing = false

  @computed get recipes() {
    return this._recipes.slice();
  }

  @computed get fetching() {
    return this._fetching
  }

  @computed get refreshing() {
    return this._refreshing
  }
}

export default BaseFetcherStore