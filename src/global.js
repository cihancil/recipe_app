import { Platform, Dimensions } from 'react-native';
import { NavigationStyles } from '@expo/ex-navigation';

const ios = Platform.OS === "ios";

const colors = {
  darkest: "#391C04",
  dark: "#543113",
  main: "#F3DFAB",
  background: "#FAF3DE",
  accent: "#FFBF00",
  imageBackground: "#f3eee0",
  starColor: "#ffbf00",
};

const fonts = {
  main: "OpenSans",
  header: "Lora",
};

const sizes = {
  screenWidth: Dimensions.get("window").width,
};

const styles = {
  shadow: {
    shadowColor: colors.dark,
    shadowOpacity: 0.3,
    shadowRadius: 2,
    shadowOffset: {
      height: 1,
      width: 0
    }
  },
  barBackground: {
    flex: 1,
    backgroundColor: colors.main,
  },
  navigator: {
    gestures: (params) => {
      const newParams = { ...params };
      newParams.gestureResponseDistance = 90;
      return NavigationStyles.SlideHorizontal.gestures(newParams);
    },
  }
};

export default {
  colors,
  fonts,
  sizes,
  styles,
}