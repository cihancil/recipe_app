import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Animated,
  Text,
  Platform,
} from 'react-native';

import Touchable from './touchable';
import Global from '../global';


export default class CategoryItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animatedFade: new Animated.Value(0.6),
    }
  }

  componentDidMount() {
    if (Platform.OS === "ios") {
      Animated.timing(
        this.state.animatedFade,
        {
          toValue: 1,
          useNativeDriver: true,
          duration: 400,
        }
      ).start();
    }
  }

  render() {
    let animatedStyle = {};
    if (Platform.OS === "ios") {
      animatedStyle = {
        opacity: this.state.animatedFade,
        transform: [{
          translateY: this.state.animatedFade.interpolate({
            inputRange: [0.6, 1],
            outputRange: [36, 0]
          }),
        }],
      };
    }
    return (
      <Animated.View
        style={[{

        }, animatedStyle]}
      >
        <Touchable style={{
          padding: 16,
          //paddingVertical: 24,
        }}
          onPress={this.props.onPress}
        >
          <Text
            style={{
              fontFamily: Global.fonts.main,
              color: Global.colors.dark,
              fontSize: 18,
            }}
          >{this.props.category.name}</Text>
        </Touchable>
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({

});