import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  TouchableNativeFeedback,
  Platform,
  Text,
} from 'react-native';

export default class Touchable extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
    if (Platform.OS === "ios") {
      return this.renderIOS();
    }
    if (Platform.OS === "android") {
      return this.renderAndroid();
    }
  }

  renderIOS() {
    return (
      <TouchableOpacity
        {...this.props}
        activeOpacity={0.8}
      >
        {this.props.children}
      </TouchableOpacity>
    );
  }

  renderAndroid() {
    return (
      <TouchableOpacity
        {...this.props}
        activeOpacity={0.8}
      >
        {this.props.children}
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
});