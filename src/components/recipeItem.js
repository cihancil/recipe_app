import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  Dimensions,
  Animated,
  Platform,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import Touchable from './touchable';
import Global from '../global';
import ReviewStars from './reviewStars';

const paddingHorizontal = 8;
const pageWidth = Dimensions.get("window").width;

export default class RecipeItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animatedFade: new Animated.Value(0.6),
    }
    this.imageWidth = pageWidth - 2 * paddingHorizontal;
    if (props.shape === "headline") {
      this.imageWidth = this.imageWidth - 16;
    }
    if (props.shape === "small") {
      this.imageWidth = (pageWidth / 2) - (1.5 * paddingHorizontal)
    }
    if (props.horizontalScroll) {
      this.imageWidth = this.imageWidth - 30;
    }
  }

  componentDidMount() {
    if (Platform.OS === "ios") {
      Animated.timing(
        this.state.animatedFade,
        {
          toValue: 1,
          useNativeDriver: true,
          duration: 400,
        }
      ).start();
    }
  }

  render() {
    let { recipe } = this.props;
    let animatedStyle = {};
    if (Platform.OS === "ios") {
      animatedStyle = {
        opacity: this.state.animatedFade,
        transform: [{
          translateY: this.state.animatedFade.interpolate({
            inputRange: [0.6, 1],
            outputRange: [36, 0]
          }),
        }],
      };
    }
    return (
      <Animated.View
        style={[{
          flex: 1,
        }, animatedStyle]}
      >
        <Touchable
          onPress={this.props.onPress}
          style={[styles.container, styles.touchable, this.props.style]}
        >
          <Image
            resizeMode="cover"
            source={{ uri: recipe.thumbnails.large }}
            style={[styles.cover, {
              width: this.imageWidth,
              height: this.imageWidth / 1.37,
            }]}
          />
          <View style={{
            backgroundColor: "white",
            padding: 8,
            width: this.imageWidth,
            flex: 1,
          }}>
            <View
              style={{
                paddingVertical: 4,
                flexDirection: this.props.shape === "small" ? "column" : "row",
                alignItems: this.props.shape === "small" ? "flex-start" : "center",
                justifyContent: this.props.shape === "small" ? "flex-start" : "space-between",
              }}
            >
              <Text
                style={{
                  fontSize: 15,
                  color: Global.colors.dark,
                  fontFamily: Global.fonts.main,
                  marginBottom: this.props.shape === "small" ? 4 : 0,
                }}
              >{recipe.categoryName}</Text>
              <ReviewStars star={recipe.stats.rating.total} />
            </View>
            <View
              style={{
                justifyContent: "flex-start",
                alignSelf: "flex-start",
              }}
            >
              <Text
                numberOfLines={this.props.horizontalScroll ? 3 : 4}
                style={styles.recipeTitle}
              >{recipe.title}</Text>
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: "flex-end",
                flexDirection: this.props.shape === "small" ? "column" : "row",
              }}
            >
              <View
                style={[{
                  flexDirection: "row",
                  alignItems: "center",
                  paddingVertical: 4,
                }, this.props.shape === "small" ? {} : {
                  flex: 5,
                }]}
              >
                <Image
                  source={{ uri: recipe.user.avatars ? recipe.user.avatars.standart : recipe.user.avatar }}
                  style={styles.userAvatar}
                />
                <View
                  style={{
                    paddingRight: 4,
                    flex: 1,
                  }}
                >
                  <Text
                    ellipsizeMode={"tail"}
                    style={styles.usernameText}
                  >{recipe.user.name}</Text>
                </View>
              </View>
              <View
                style={[{
                  padding: 8,
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between"
                }, this.props.shape === "small" ? {} : {
                  flex: 3,
                }]}
              >
                <Icon name="eye" color={Global.colors.darkest} />
                <Text style={{
                  marginLeft: 4,
                  color: Global.colors.darkest,
                  fontFamily: Global.fonts.main,
                }}>{recipe.stats.viewCount}</Text>
                <View style={{ flex: 1 }} />
                <Icon name="heart" color={Global.colors.darkest} />
                <Text style={{
                  marginLeft: 4,
                  color: Global.colors.darkest,
                  fontFamily: Global.fonts.main,
                }}>{recipe.stats.favoriteCount}</Text>
              </View>

            </View>
          </View>
        </Touchable>
      </Animated.View >

    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal,
    alignItems: "stretch",
    justifyContent: "flex-start",
  },
  touchable: {
    paddingVertical: 8,
    flex: 1,
  },
  cover: {
    backgroundColor: Global.colors.imageBackground,
  },
  recipeTitle: {
    fontFamily: Global.fonts.header,
    fontWeight: "bold",
    fontSize: 18,
    color: Global.colors.dark,
    marginVertical: 4,
  },
  userAvatar: {
    width: 36, height: 36, borderRadius: 18, marginRight: 4,
  },
  usernameText: {
    fontFamily: Global.fonts.main,
    color: Global.colors.dark,
  },
});