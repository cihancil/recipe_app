import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  ActivityIndicator,
  RefreshControl,
  Platform,
  ListView,
} from 'react-native';

import Global from '../global';

export default class FeedView extends Component {
  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      dataSource: ds.cloneWithRows([])
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.source !== this.props.source) {
      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(nextProps.source)
      })
    }
  }

  render() {
    return (
      <ListView
        {...this.props }
        enableEmptySections={true}
        showsVerticalScrollIndicator={false}
        pageSize={12}
        scrollRenderAheadDistance={900}
        dataSource={this.state.dataSource}
        renderFooter={() => {
          return (this.props.canFetchMore || this.props.fetching)
            ? <ActivityIndicator
              style={{ paddingVertical: 12, transform: [{ scale: 0.82 }] }}
              size="large"
              color={Global.colors.dark}
            />
            : null
        }}
      />
    )
  }
}
