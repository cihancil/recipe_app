import React, { Component } from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import Global from '../global';

export default class ReviewStars extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
    this.fullStar = parseInt(props.star);
    this.hasHalfStar = parseFloat(props.star) % 1 !== 0;
    this.emptyStar = 5 - this.fullStar;
    if (this.hasHalfStar) {
      this.emptyStar = this.emptyStar - 1;
    }
  }

  render() {
    let count = this.fullStar;
    return (
      <View style={[styles.container, this.props.style]}>
        {
          this.fullStar >= 1 &&
          <Icon
            color={Global.colors.starColor}
            name={"star"} />
        }
        {
          this.fullStar >= 2 &&
          <Icon
            color={Global.colors.starColor}
            name={"star"} />
        }
        {
          this.fullStar >= 3 &&
          <Icon
            color={Global.colors.starColor}
            name={"star"} />
        }
        {
          this.fullStar >= 4 &&
          <Icon
            color={Global.colors.starColor}
            name={"star"} />
        }
        {
          this.fullStar >= 5 &&
          <Icon
            color={Global.colors.starColor}
            name={"star"} />
        }
        {
          this.hasHalfStar &&
          <Icon
            color={Global.colors.starColor}
            name={"star-half-o"} />
        }
        {
          this.emptyStar >= 1 &&
          <Icon
            color={Global.colors.starColor}
            name={"star-o"} />
        }
        {
          this.emptyStar >= 2 &&
          <Icon
            color={Global.colors.starColor}
            name={"star-o"} />
        }
        {
          this.emptyStar >= 3 &&
          <Icon
            color={Global.colors.starColor}
            name={"star-o"} />
        }
        {
          this.emptyStar >= 4 &&
          <Icon
            color={Global.colors.starColor}
            name={"star-o"} />
        }
        {
          this.emptyStar >= 5 &&
          <Icon
            color={Global.colors.starColor}
            name={"star-o"} />
        }

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
  }
});