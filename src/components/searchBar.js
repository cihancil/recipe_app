import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  Text,
  Platform,
} from 'react-native';

import Touchable from './touchable';

export default class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: "",
    }
  }

  render() {
    return (
      <View
        style={[styles.searchBarContainer, this.props.style]}
      >

        <TextInput
          style={styles.searchBar}
          autoCorrect={false}
          autoCapitalize={"none"}
          autoFocus={false}
          returnKeyType={"search"}
          placeholder={"Tarif ve Mutfak Ara..."}
          placeholderTextColor="gray"
          enablesReturnKeyAutomatically={true}
          spellCheck={false}
          onChangeText={(text) => this.setState({ text })}
          onSubmitEditing={this._handleSearch.bind(this)}
          value={this.state.text}
        />
        <Touchable
          style={styles.searchButton}
          onPress={this._handleSearch.bind(this)}
        >
          <Text>ARA</Text>
        </Touchable>
      </View>
    );
  }

  _handleSearch() {
    let searchText = this.state.text;
    this.setState({ text: "" }, () => {
      this.props.onSearch(searchText)
    });
  }
}

const styles = StyleSheet.create({
  searchBarContainer: {
    flexDirection: "row",
    paddingHorizontal: 8,
  },
  searchBar: {
    height: 48,
    flex: 1,
    padding: 8,
    backgroundColor: Platform.OS === "android" ? "transparent" : "white",
    borderRadius: 4,
    borderWidth: Platform.OS === "android" ? 0 : 1,
  },
  searchButton: {
    backgroundColor: "orange",
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: 16,
    borderRadius: 4,
    marginLeft: 8,
  }
});