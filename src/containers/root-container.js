import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  StatusBar,
} from 'react-native';
import SplashScreen from 'react-native-smart-splash-screen'

import Global from '../global';
import NavigationContainer from './navigation-container';

export default class RootContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  componentDidMount() {
    SplashScreen.close({
      animationType: SplashScreen.animationType.scale,
      duration: 400,
      delay: 200,
    })
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          barStyle="default"
          backgroundColor={Global.colors.dark}
        />
        <NavigationContainer />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
});