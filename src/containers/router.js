import {
  createRouter,
} from '@expo/ex-navigation';

import HomeScreen from '../screens/homeScreen';
import CategoriesScreen from '../screens/categoriesScreen';
import SearchScreen from '../screens/searchScreen';
import ProfileScreen from '../screens/profileScreen';
import DetailScreen from '../screens/detailScreen';
import CategoryFeedScreen from '../screens/categoryFeedScreen';
import SearchFeedScreen from '../screens/searchFeedScreen';
import TabContainer from './tab-container';

const Router = createRouter(() => ({
  Main: () => TabContainer,
  Home: () => HomeScreen,
  Categories: () => CategoriesScreen,
  Search: () => SearchScreen,
  Profile: () => ProfileScreen,
  Detail: () => DetailScreen,
  CategoryFeed: () => CategoryFeedScreen,
  SearchFeed: () => SearchFeedScreen,
}), { ignoreSerializableWarnings: true });

const Routes = {
  Main: "Main",
  Home: "Home",
  Categories: "Categories",
  Search: "Search",
  Profile: "Profile",
  Detail: "Detail",
  CategoryFeed: "CategoryFeed",
  SearchFeed: "SearchFeed",
}

export default Router;
export {
  Routes,
};