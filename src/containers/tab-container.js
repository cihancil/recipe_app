import React, { Component } from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';
import {
  StackNavigation,
  TabNavigation,
  TabNavigationItem as TabItem,
} from '@expo/ex-navigation';

import Global from '../global';
import Router, { Routes } from './router';
import Icon from 'react-native-vector-icons/FontAwesome';


export default class TabsScreen extends Component {
  static route = {
    navigationBar: {
      visible: false,
    }
  }
  render() {
    return (
      <TabNavigation
        id="Main"
        navigatorUID="main"
        initialTab="home"
      >
        <TabItem
          id="home"
          style={styles.tab}
          selectedStyle={styles.tab}
          renderIcon={(isSelected) => <Icon name="cutlery" size={28}
            color={isSelected
              ? Global.colors.accent
              : Global.colors.main} />}>
          <StackNavigation
            id="Home"
            navigatorUID="home"
            initialRoute={Router.getRoute(Routes.Home)}
          />
        </TabItem>

        <TabItem
          id="Categories"
          style={styles.tab}
          selectedStyle={styles.tab}
          renderIcon={(isSelected) => <Icon name="list-ul" size={28}
            color={isSelected
              ? Global.colors.accent
              : Global.colors.main} />}>
          <StackNavigation
            id="Categories"
            initialRoute={Router.getRoute(Routes.Categories)}
          />
        </TabItem>

        <TabItem
          id="Search"
          style={styles.tab}
          selectedStyle={styles.tab}
          renderIcon={(isSelected) => <Icon name="search" size={28} color={isSelected
            ? Global.colors.accent
            : Global.colors.main} />}>
          <StackNavigation
            id="Search"
            initialRoute={Router.getRoute(Routes.Search)}
          />
        </TabItem>
      </TabNavigation >
    );
  }
}


const styles = StyleSheet.create({
  tab: {
    backgroundColor: Global.colors.dark,
  }
});