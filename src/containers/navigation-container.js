import React, { Component } from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';
import {
  createRouter,
  NavigationProvider,
  StackNavigation,
  TabNavigation,
  TabNavigationItem as TabItem,
} from '@expo/ex-navigation';
import { observer, inject } from 'mobx-react/native';

import Router, { Routes } from './router';

export default class NavigationContainer extends Component {
  render() {
    return (
      <NavigationProvider router={Router}>
        <StackNavigation initialRoute={Router.getRoute(Routes.Main)} />
      </NavigationProvider>
    )
  }
}