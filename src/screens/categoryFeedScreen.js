import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
  InteractionManager,
} from 'react-native';
import { inject, observer } from 'mobx-react/native';

import Global from '../global';
import { Routes } from '../containers/router';
import FeedView from '../components/feedView';
import RecipeItem from '../components/recipeItem';
import CategoryFeedStore from '../stores/category-feed-store';

@inject("store")
@observer
export default class CategoryFeedScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
    this.category = this.props.category;
    this.feed = new CategoryFeedStore(this.category.id);
  }

  static route = {
    navigationBar: {
      visible: true,
      title: (params => {
        return params.category.name;
      }),
      tintColor: Global.colors.dark,
    },
    styles: Global.styles.navigator,
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      this.feed.fetchCategoryFeed()
    })
  }

  render() {
    return (
      <View style={styles.container}>
        <FeedView
          source={this.feed.recipes}
          renderRow={this.renderItem.bind(this)}
          fetching={this.feed.fetching}
          refreshing={this.feed.refreshing}
          canFetchMore={this.feed.canFetchMore}
          onEndReachedThreshold={80}
          onEndReached={() => {
            this.feed.fetchCategoryFeed()
          }}
        />
      </View>
    )
  }


  renderItem(item, sectionIndex, index) {
    return (
      <RecipeItem
        key={"category_feed_" + index}
        recipe={item}
        shape="big"
        onPress={() => {
          this.props.navigator.push(Routes.Detail, {
            recipe: item,
          });
        }}
      />
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Global.colors.background,
  }
});