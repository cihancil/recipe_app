import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Platform,
  Text,
} from 'react-native';
import { inject, observer } from 'mobx-react/native';

import Global from '../global';
import { Routes } from '../containers/router';
import FeedView from '../components/feedView';
import RecipeItem from '../components/recipeItem';

@inject("store")
@observer
export default class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  static route = {
    navigationBar: {
      visible: true,
      renderTitle: (route, props) =>
        <Image
          style={styles.logo}
          resizeMode="contain"
          source={require("../../assets/images/ly_logo.png")}
        />,
      renderBackground: (route, props) =>
        <View style={[Global.styles.barBackground, Global.styles.shadow]} />
    }
  }

  componentDidMount() {
    this.props.store.home.fetchHomeFeed();
  }

  render() {
    return (
      <View
        style={styles.container}
      >
        <FeedView
          source={this.props.store.home.recipes}
          renderRow={this.renderItem.bind(this)}
          fetching={this.props.store.home.fetching}
          refreshing={this.props.store.home.refreshing}
          onRefresh={() => {
            this.props.store.home.refreshHomeFeed()
          }}
        />
      </View >
    );
  }

  renderItem(item, sectionIndex, index) {
    if (item.type === "headlines") {
      return (
        <View
          key={"homefeed_" + index}
          style={styles.headlinesContainer}
        >
          {item.recipes.map((headline, headlineIndex) => {
            return (
              <RecipeItem
                key={"headline_" + headlineIndex}
                recipe={headline}
                shape="headline"
                onPress={this._navigateToDetail.bind(this, headline)}
              />
            )
          })}
        </View>
      )
    }

    if (item.type === "single") {
      return (
        <RecipeItem
          key={"homefeed_" + index}
          recipe={item.recipe}
          shape="big"
          onPress={this._navigateToDetail.bind(this, item.recipe)}
        />
      )
    }

    if (item.type === "double") {
      return (
        <View
          key={"homefeed_" + index}
          style={styles.recipeRowContainer}
        >
          <RecipeItem
            recipe={item.recipes[0]}
            shape="small"
            style={{ paddingRight: 4, }}
            onPress={this._navigateToDetail.bind(this, item.recipes[0])}
          />
          {
            item.recipes[1] &&
            <RecipeItem
              recipe={item.recipes[1]}
              shape="small"
              style={{ paddingLeft: 4 }}
              onPress={this._navigateToDetail.bind(this, item.recipes[1])}
            />
          }
        </View>
      )
    }
    return null;
  }

  _navigateToDetail(recipe) {
    this.props.navigator.push(Routes.Detail, {
      recipe,
    });
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Global.colors.background,
  },
  logo: {
    flex: 1,
    marginLeft: Platform.OS === "android" ? 16 : 0,
    alignSelf: "center",
  },
  headlinesContainer: {
    backgroundColor: Global.colors.darkest,
    padding: 8,
    paddingVertical: 8,
    marginBottom: 8,
  },
  recipeRowContainer: {
    flexDirection: "row",
  },
});