import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
} from 'react-native';

import Global from '../global';

const width = Dimensions.get("window").width;

export default class DetailScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  static route = {
    navigationBar: {
      visible: true,
      title: (params => {
        return params.recipe.title;
      }),
      titleStyle: {
        marginHorizontal: 12,
      },
      tintColor: Global.colors.dark,
    },
    styles: Global.styles.navigator,
  }

  render() {
    return <View style={styles.container} />
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Global.colors.background,
  }
});