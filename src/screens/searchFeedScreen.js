import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
  InteractionManager,
} from 'react-native';
import { inject, observer } from 'mobx-react/native';

import Global from '../global';
import { Routes } from '../containers/router';
import FeedView from '../components/feedView';
import SearchFeedStore from '../stores/search-feed-store';
import RecipeItem from '../components/recipeItem';

@inject("store")
@observer
export default class SearchFeedScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
    this.searchKey = this.props.searchKey;
    this.feed = new SearchFeedStore(this.searchKey);
  }

  static route = {
    navigationBar: {
      visible: true,
      title: (params => {
        return params.searchKey;
      }),
      tintColor: Global.colors.dark,
    },
    styles: Global.styles.navigator,
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      this.feed.fetchSearchFeed()
    })
  }

  render() {
    return (
      <View style={styles.container}>
        <FeedView
          source={this.feed.recipes}
          renderRow={this.renderItem.bind(this)}
          fetching={this.feed.fetching}
          refreshing={this.feed.refreshing}
          canFetchMore={this.feed.canFetchMore}
          onEndReachedThreshold={80}
          onEndReached={() => {
            this.feed.fetchSearchFeed()
          }}
        />
      </View>
    )
  }


  renderItem(item, sectionIndex, index) {
    return (
      <RecipeItem
        key={"category_feed_" + index}
        recipe={item}
        shape="big"
        onPress={() => {
          this.props.navigator.push(Routes.Detail, {
            recipe: item,
          });
        }}
      />
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Global.colors.background,
  }
});