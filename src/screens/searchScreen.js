import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  InteractionManager,
  Text,
  ScrollView,
} from 'react-native';
import { inject, observer } from 'mobx-react/native';

import SearchStore from '../stores/search-store';
import Global from '../global';
import { Routes } from '../containers/router';
import SearchBar from '../components/searchBar';
import FeedView from '../components/feedView';
import RecipeItem from '../components/recipeItem';

@inject("store")
@observer
export default class SearchScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  static route = {
    navigationBar: {
      visible: true,
      title: "Ara",
      tintColor: Global.colors.dark,
      renderBackground: (route, props) =>
        <View style={[Global.styles.barBackground, Global.styles.shadow]} />
    }
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      this.props.store.search.fetchDiscovery()
    })
  }

  render() {
    return (
      <View style={styles.container}>
        <SearchBar
          style={{
            marginBottom: 4,
          }}
          onSearch={(searchText) => {
            if (searchText !== "") {
              this.props.navigator.push(Routes.SearchFeed, { searchKey: searchText });
            }
          }}
        />
        <FeedView
          source={this.props.store.search.discoveryItems}
          renderRow={this.renderItem.bind(this)}
          fetching={this.props.store.search.fetching}
        />
      </View>
    );
  }

  renderItem(item, sectionIndex, index) {
    if (item.type === "recipe") {
      return (
        <View>
          <View style={styles.recipeCategoryContainer} >
            <Text style={styles.recipeCategoryTitle}>{item.title}</Text>
          </View>
          <ScrollView
            contentContainerStyle={styles.recipesContainer}
            horizontal={true}
          >
            {
              item.data.map((recipe, index) => {
                return (
                  <RecipeItem
                    key={"search_screen_recipe_item_" + recipe.sequence}
                    recipe={item.data[index]}
                    shape="small"
                    horizontalScroll={true}
                    style={styles.smallRecipeItem}
                    onPress={() => {
                      this.props.navigator.push(Routes.Detail, {
                        recipe,
                      });
                    }}
                  />
                )
              })
            }
          </ScrollView>
        </View>
      )
    }
    return null;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Global.colors.background,
    paddingTop: 4,
  },
  recipeCategoryContainer: {
    justifyContent: "center",
    borderBottomColor: Global.colors.dark,
    borderBottomWidth: 3,
    marginHorizontal: 8,
    padding: 4,
  },
  recipeCategoryTitle: {
    fontSize: 20,
    color: Global.colors.dark,
    fontFamily: Global.fonts.header,
    fontWeight: "bold",
  },
  recipesContainer: {
    flexDirection: "row",
    height: 300,
  },
  smallRecipeItem: {
    paddingRight: 4
  },
});