import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
} from 'react-native';
import { inject, observer } from 'mobx-react/native';

import Global from '../global';
import { Routes } from '../containers/router';
import FeedView from '../components/feedView';
import CategoryItem from '../components/categoryItem';

@inject("store")
@observer
export default class CategoriesScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  static route = {
    navigationBar: {
      visible: true,
      title: "Kategoriler",
      tintColor: Global.colors.dark,
      renderBackground: (route, props) =>
        <View style={[Global.styles.barBackground, Global.styles.shadow]} />
    }
  }

  componentDidMount() {
    this.props.store.categories.fetchCategories();
  }

  render() {
    let { categories: categoriesStore } = this.props.store;
    return (
      <View style={styles.container}>
        <FeedView
          source={categoriesStore.categories}
          renderRow={this.renderItem.bind(this)}
          fetching={categoriesStore.fetching}
          refreshing={categoriesStore.refreshing}
        />
      </View>
    );
  }

  renderItem(item, sectionIndex, index) {
    return (
      <CategoryItem
        category={item}
        onPress={() => {
          this.props.navigator.push(Routes.CategoryFeed, { category: item });
        }}
      />
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Global.colors.background,
  }
});