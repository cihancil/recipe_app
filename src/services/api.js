const _request = (body) => {
  let url = "https://www.lezizyemeklerim.com/api";
  body["Content-Type"] = "mobile-app:application/json";
  let opts = {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
  };

  return fetch(url, opts)
    .then(response => {
      return response.json()
    })
    .catch(err => {
      console.log("fetch error", err)
    })
};


export default {
  fetchHomeFeed() {
    return _request({
      action: "homepage"
    })
  },
  fetchCategories() {
    return _request({
      action: "categoryList"
    })
  },
  fetchCategoryFeed({ id, page }) {
    return _request({
      action: "category",
      id,
      page,
    })
  },
  fetchDiscovery() {
    return _request({
      action: "searchDiscovery",
    })
  },
  fetchSearchResults({ wildcard, page }) {
    return _request({
      action: "search",
      wildcard,
      page,
    })
  }
}